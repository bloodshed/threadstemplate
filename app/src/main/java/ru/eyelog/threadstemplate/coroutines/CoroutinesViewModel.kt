package ru.eyelog.threadstemplate.coroutines

import androidx.lifecycle.*
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.datasource.ThreadModel
import ru.eyelog.threadstemplate.datasource.ThreadsModel
import ru.eyelog.threadstemplate.rxsingle.RxSingleViewModel
import ru.eyelog.threadstemplate.threadmain.ThreadMainViewModel
import ru.eyelog.threadstemplate.threadsubnew.ThreadSubNewViewModel
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldViewModel

class CoroutinesViewModel (private val dataSource: FakeDataSource): ViewModel() {

    private val _titleLiveData = MutableLiveData<List<ThreadModel>>()
    val titleLiveData: LiveData<List<ThreadModel>> = _titleLiveData

    private val _showLoaderLiveData = MutableLiveData<Unit>()
    val showLoaderLiveData: LiveData<Unit> = _showLoaderLiveData

    private val _hideLoaderLiveData = MutableLiveData<Unit>()
    val hideLoaderLiveData: LiveData<Unit> = _hideLoaderLiveData

    fun requestData() {
        // TODO not integrated with viewModel coroutine
//        GlobalScope.launch(Dispatchers.IO){
//            _titleLiveData.postValue(dataSource.getSomeData().models)
//        }

        _showLoaderLiveData.postValue(Unit)

        viewModelScope.launch(Dispatchers.IO){
            val gotData = dataSource.getSomeData()
            _titleLiveData.postValue(gotData.models)
            _hideLoaderLiveData.postValue(Unit)
        }
    }
}