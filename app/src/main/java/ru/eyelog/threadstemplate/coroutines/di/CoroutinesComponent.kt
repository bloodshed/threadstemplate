package ru.eyelog.threadstemplate.coroutines.di

import dagger.Component
import ru.eyelog.threadstemplate.coroutines.CoroutinesFragment
import ru.eyelog.threadstemplate.rxsingle.RxSingleFragment

@Component(modules = [CoroutinesModule::class])
interface CoroutinesComponent {

    @Component.Factory
    interface Factory{
        fun create(): CoroutinesComponent
    }

    fun inject(fragment: CoroutinesFragment)

}