package ru.eyelog.threadstemplate.coroutines.di

import dagger.Module
import dagger.Provides
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.coroutines.CoroutinesViewModel
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.rxsingle.RxSingleViewModel
import ru.eyelog.threadstemplate.threadmain.ThreadMainViewModel
import ru.eyelog.threadstemplate.threadsubnew.ThreadSubNewViewModel
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldViewModel

@Module
class CoroutinesModule {

    @Provides
    fun getDataSource() = FakeDataSource()

    @Provides
    fun getAdapter() = ThreadAdapter()

    @Provides
    fun getViewModel(dataSource: FakeDataSource) = CoroutinesViewModel(dataSource)
}