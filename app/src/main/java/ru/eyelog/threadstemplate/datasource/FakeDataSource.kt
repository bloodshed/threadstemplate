package ru.eyelog.threadstemplate.datasource

class FakeDataSource {

    fun getSomeData(): ThreadsModel{

        Thread.sleep(5000L)

        val outList = mutableListOf<ThreadModel>()

        for (i in 0..5){
            outList.add(
                ThreadModel(
                    number = i,
                    title = "Delayed item",
                    delayValue = "5000L"
                )
            )
        }
        return ThreadsModel(outList.toList())
    }
}