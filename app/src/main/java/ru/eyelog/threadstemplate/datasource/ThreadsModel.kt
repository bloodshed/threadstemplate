package ru.eyelog.threadstemplate.datasource

import java.io.Serializable

data class ThreadsModel(
    val models: List<ThreadModel>
) : Serializable
