package ru.eyelog.threadstemplate.datasource

import java.io.Serializable

data class ThreadModel(
    val number: Int,
    val title: String,
    val delayValue: String
): Serializable

