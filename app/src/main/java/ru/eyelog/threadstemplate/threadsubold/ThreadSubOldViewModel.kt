package ru.eyelog.threadstemplate.threadsubold

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import androidx.lifecycle.*
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.datasource.ThreadModel
import ru.eyelog.threadstemplate.datasource.ThreadsModel

class ThreadSubOldViewModel(private val dataSource: FakeDataSource): ViewModel(),
    LifecycleEventObserver {

    private val _titleLiveData = MutableLiveData<List<ThreadModel>>()
    val titleLiveData: LiveData<List<ThreadModel>> = _titleLiveData

    private val _showLoaderLiveData = MutableLiveData<Unit>()
    val showLoaderLiveData: LiveData<Unit> = _showLoaderLiveData

    private val _hideLoaderLiveData = MutableLiveData<Unit>()
    val hideLoaderLiveData: LiveData<Unit> = _hideLoaderLiveData

    lateinit var thread: Thread

    private val handler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            val bundle = msg.data
            val data = bundle.getSerializable("Key") as? ThreadsModel
            data?.let {
                _hideLoaderLiveData.postValue(Unit)
                _titleLiveData.postValue(data.models)
            }
        }
    }

    fun requestData() {
        _showLoaderLiveData.postValue(Unit)
        val runnable = Runnable {
            val gotData = dataSource.getSomeData()
            val msg: Message = handler.obtainMessage()
            val bundle = Bundle()
            bundle.putSerializable("Key", gotData)
            msg.data = bundle
            handler.sendMessage(msg)
        }
        thread = Thread(runnable)
        thread.start()
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when(event){
            Lifecycle.Event.ON_DESTROY -> {
                thread.interrupt()
            }
            else -> {}
        }
    }
}