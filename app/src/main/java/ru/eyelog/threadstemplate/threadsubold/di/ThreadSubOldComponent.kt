package ru.eyelog.threadstemplate.threadsubold.di

import dagger.Component
import ru.eyelog.threadstemplate.threadmain.ThreadMainFragment
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldFragment

@Component(modules = [ThreadSubOldModule::class])
interface ThreadSubOldComponent {

    @Component.Factory
    interface Factory{
        fun create(): ThreadSubOldComponent
    }

    fun inject(fragment: ThreadSubOldFragment)

}