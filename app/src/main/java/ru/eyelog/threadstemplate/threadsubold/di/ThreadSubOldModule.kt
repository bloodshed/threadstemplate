package ru.eyelog.threadstemplate.threadsubold.di

import dagger.Module
import dagger.Provides
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.threadmain.ThreadMainViewModel
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldViewModel

@Module
class ThreadSubOldModule {

    @Provides
    fun getDataSource() = FakeDataSource()

    @Provides
    fun getAdapter() = ThreadAdapter()

    @Provides
    fun getViewModel(dataSource: FakeDataSource) = ThreadSubOldViewModel(dataSource)
}