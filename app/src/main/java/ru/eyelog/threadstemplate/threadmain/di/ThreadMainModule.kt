package ru.eyelog.threadstemplate.threadmain.di

import dagger.Module
import dagger.Provides
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.threadmain.ThreadMainViewModel

@Module
class ThreadMainModule {

    @Provides
    fun getDataSource() = FakeDataSource()

    @Provides
    fun getAdapter() = ThreadAdapter()

    @Provides
    fun getViewModel(dataSource: FakeDataSource) = ThreadMainViewModel(dataSource)
}