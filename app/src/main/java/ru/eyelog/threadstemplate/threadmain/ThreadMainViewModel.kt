package ru.eyelog.threadstemplate.threadmain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.datasource.ThreadModel

class ThreadMainViewModel(private val dataSource: FakeDataSource): ViewModel() {

    private val _titleLiveData = MutableLiveData<List<ThreadModel>>()
    val titleLiveData: LiveData<List<ThreadModel>> = _titleLiveData

    fun requestData() {
        val gotData = dataSource.getSomeData()
        _titleLiveData.postValue(gotData.models)
    }
}