package ru.eyelog.threadstemplate.threadmain

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list.*
import ru.eyelog.threadstemplate.R
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.threadmain.di.DaggerThreadMainComponent
import javax.inject.Inject

class ThreadMainFragment: Fragment() {

    @Inject
    lateinit var adapter: ThreadAdapter

    @Inject
    lateinit var viewModel: ThreadMainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerThreadMainComponent
            .create()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitle.text = getString(R.string.threads_main)

        rvList.layoutManager = LinearLayoutManager(requireContext())
        rvList.adapter = adapter

        viewModel.titleLiveData.observe(viewLifecycleOwner) {
            adapter.setContent(it)
        }

        btTap.setOnClickListener {
            viewModel.requestData()
        }
    }
}