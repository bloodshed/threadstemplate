package ru.eyelog.threadstemplate.threadmain.di

import dagger.Component
import ru.eyelog.threadstemplate.threadmain.ThreadMainFragment

@Component(modules = [ThreadMainModule::class])
interface ThreadMainComponent {

    @Component.Factory
    interface Factory{
        fun create(): ThreadMainComponent
    }

    fun inject(fragment: ThreadMainFragment)

}