package ru.eyelog.threadstemplate.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.eyelog.threadstemplate.R
import ru.eyelog.threadstemplate.datasource.ThreadModel

class ThreadAdapter : RecyclerView.Adapter<ThreadAdapter.ThreadElementHolder>() {

    var elementList = listOf<ThreadModel>()

    fun setContent(content: List<ThreadModel>) {
        elementList = content
        notifyItemRangeChanged(0, content.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThreadElementHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return ThreadElementHolder(itemView)
    }

    override fun onBindViewHolder(holder: ThreadElementHolder, position: Int) {
        holder.number.text = elementList[position].number.toString()
        holder.title.text = elementList[position].title
        holder.delayValue.text = elementList[position].delayValue
    }

    override fun getItemCount() = elementList.size

    class ThreadElementHolder(
        item: View
    ) : RecyclerView.ViewHolder(item) {

        val number: TextView = item.findViewById(R.id.tvNumber)
        val title: TextView = item.findViewById(R.id.tvTitle)
        val delayValue: TextView = item.findViewById(R.id.tvDelayValue)
    }
}