package ru.eyelog.threadstemplate.rxsingle

import androidx.lifecycle.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.datasource.ThreadModel
import ru.eyelog.threadstemplate.datasource.ThreadsModel

class RxSingleViewModel (private val dataSource: FakeDataSource): ViewModel(),
    LifecycleEventObserver {

    private val _titleLiveData = MutableLiveData<List<ThreadModel>>()
    val titleLiveData: LiveData<List<ThreadModel>> = _titleLiveData

    private val _showLoaderLiveData = MutableLiveData<Unit>()
    val showLoaderLiveData: LiveData<Unit> = _showLoaderLiveData

    private val _hideLoaderLiveData = MutableLiveData<Unit>()
    val hideLoaderLiveData: LiveData<Unit> = _hideLoaderLiveData


    private var disposable: Disposable? = null

    private val observer = Single.create<ThreadsModel> { e ->
        e.onSuccess(dataSource.getSomeData())
    }

    fun requestData() {
        disposable = observer
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _showLoaderLiveData.postValue(Unit)
            }
            .doFinally {
                _hideLoaderLiveData.postValue(Unit)
            }
            .subscribe { data ->
                _titleLiveData.postValue(data.models)
            }
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when(event){
            Lifecycle.Event.ON_DESTROY -> {
                disposable?.dispose()
            }
            else -> {}
        }
    }
}