package ru.eyelog.threadstemplate.rxsingle.di

import dagger.Component
import ru.eyelog.threadstemplate.rxsingle.RxSingleFragment

@Component(modules = [RxSingleModule::class])
interface RxSingleComponent {

    @Component.Factory
    interface Factory{
        fun create(): RxSingleComponent
    }

    fun inject(fragment: RxSingleFragment)

}