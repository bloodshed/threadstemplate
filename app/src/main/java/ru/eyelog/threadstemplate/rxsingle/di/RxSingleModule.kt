package ru.eyelog.threadstemplate.rxsingle.di

import dagger.Module
import dagger.Provides
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.rxsingle.RxSingleViewModel
import ru.eyelog.threadstemplate.threadmain.ThreadMainViewModel
import ru.eyelog.threadstemplate.threadsubnew.ThreadSubNewViewModel
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldViewModel

@Module
class RxSingleModule {

    @Provides
    fun getDataSource() = FakeDataSource()

    @Provides
    fun getAdapter() = ThreadAdapter()

    @Provides
    fun getViewModel(dataSource: FakeDataSource) = RxSingleViewModel(dataSource)
}