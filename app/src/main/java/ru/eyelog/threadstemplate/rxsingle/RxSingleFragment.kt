package ru.eyelog.threadstemplate.rxsingle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.DrawableImageViewTarget
import kotlinx.android.synthetic.main.fragment_list.*
import ru.eyelog.threadstemplate.R
import ru.eyelog.threadstemplate.adapter.ThreadAdapter
import ru.eyelog.threadstemplate.rxsingle.di.DaggerRxSingleComponent
import ru.eyelog.threadstemplate.threadsubnew.ThreadSubNewViewModel
import ru.eyelog.threadstemplate.threadsubnew.di.DaggerThreadSubNewComponent
import javax.inject.Inject

class RxSingleFragment: Fragment() {

    @Inject
    lateinit var adapter: ThreadAdapter

    @Inject
    lateinit var viewModel: RxSingleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerRxSingleComponent
            .create()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(R.drawable.long_cat_mode)
            .into(DrawableImageViewTarget(ivLoadingBar))

        lifecycle.addObserver(viewModel)

        tvTitle.text = getString(R.string.rx_single)

        rvList.layoutManager = LinearLayoutManager(requireContext())
        rvList.adapter = adapter

        viewModel.titleLiveData.observe(viewLifecycleOwner) {
            adapter.setContent(it)
        }

        viewModel.showLoaderLiveData.observe(viewLifecycleOwner) {
            showLoader()
        }
        viewModel.hideLoaderLiveData.observe(viewLifecycleOwner) {
            hideLoader()
        }

        btTap.setOnClickListener {
            viewModel.requestData()
        }
    }

    private fun showLoader(){
        ivLoadingBar.visibility = View.VISIBLE
        rvList.visibility = View.GONE
    }

    private fun hideLoader(){
        ivLoadingBar.visibility = View.GONE
        rvList.visibility = View.VISIBLE
    }
}