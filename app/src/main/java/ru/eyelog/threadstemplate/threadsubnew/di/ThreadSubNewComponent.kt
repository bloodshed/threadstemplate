package ru.eyelog.threadstemplate.threadsubnew.di

import dagger.Component
import ru.eyelog.threadstemplate.threadmain.ThreadMainFragment
import ru.eyelog.threadstemplate.threadsubnew.ThreadSubNewFragment
import ru.eyelog.threadstemplate.threadsubold.ThreadSubOldFragment

@Component(modules = [ThreadSubNewModule::class])
interface ThreadSubNewComponent {

    @Component.Factory
    interface Factory{
        fun create(): ThreadSubNewComponent
    }

    fun inject(fragment: ThreadSubNewFragment)

}