package ru.eyelog.threadstemplate.threadsubnew

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.*
import ru.eyelog.threadstemplate.datasource.FakeDataSource
import ru.eyelog.threadstemplate.datasource.ThreadModel
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ThreadSubNewViewModel (private val dataSource: FakeDataSource): ViewModel(),
    LifecycleEventObserver {

    private val _titleLiveData = MutableLiveData<List<ThreadModel>>()
    val titleLiveData: LiveData<List<ThreadModel>> = _titleLiveData

    private val _showLoaderLiveData = MutableLiveData<Unit>()
    val showLoaderLiveData: LiveData<Unit> = _showLoaderLiveData

    private val _hideLoaderLiveData = MutableLiveData<Unit>()
    val hideLoaderLiveData: LiveData<Unit> = _hideLoaderLiveData

    private val executor: ExecutorService = Executors.newSingleThreadExecutor()
    private val handler = Handler(Looper.getMainLooper())

    fun requestData() {
        _showLoaderLiveData.postValue(Unit)
        executor.execute {
            val gotData = dataSource.getSomeData()
            handler.post {
                _hideLoaderLiveData.postValue(Unit)
                _titleLiveData.postValue(gotData.models)
            }
        }
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when(event){
            Lifecycle.Event.ON_DESTROY -> {
                executor.shutdown()
            }
            else -> {}
        }
    }
}